## 开始
使用 Vite 创建一个新的 React 应用程序：

```bash
npm create vite@latest name-of-your-project -- --template react
# follow prompts
cd <your new project directory>
npm install react-router-dom localforage match-sorter sort-by
npm run dev
```

## 添加 Router
创建路由，使用路由。

```jsx
import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import Root, { loader as rootLoader } from "./routes/root.jsx";
import ErrorPage from "./error-page.jsx";
import Contact from "./routes/contact.jsx";

// 创建路由
const router = createBrowserRouter([
    {
        path: '/',
        // 将<Root>设置为根路由element
        element: <Root/>,
        // 将<ErrorPage>设置为根路由上的errorElement
        errorElement: <ErrorPage/>,
        // 配置 loader
        loader: rootLoader,
        // 子路由
        children: [
            {
                path: 'contacts/:contactsId',
                element: <Contact />
            }
        ]
    },
])

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        {/* 使用路由 */}
        <RouterProvider router={router}/>
    </React.StrictMode>,
);
```
上面 main.jsx 中有很多路由配置，接下来会逐渐进行讲解。
## 根路由


创建`src/routes`和`src/routes/root.jsx`，根布局组件。并将它设置为根路由 element。
```
// 将<Root>设置为根路由element
element: <Root/>,
```

## 错误页
在渲染、加载数据或执行数据突变时出现错误，React Router 就会捕获并渲染错误页面。

useRouterError 提供了抛出异常的错误信息 导航到不存在的路由时，useRouterError().statusText 得到一个"未找到"的错误响应。

所以我们要做的是制作自己的错误页 src/error-page.jsx。

```jsx
import { useRouteError } from "react-router-dom";

// 创建错误页面组件
export default function ErrorPage() {
    // useRouterError 提供了抛出异常的错误信息
    const error = useRouteError();
    console.error(error);

    return (
        <div id="error-page">
            <h1>Oops!</h1>
            <p>Sorry, an unexpected error has occurred.</p>
            <p>
                {/*导航到不存在的路由时，得到一个"未找到"的错误响应 statusText*/}
                <i>{error.statusText || error.message}</i>
            </p>
        </div>
    );
}
```
并设置为根路由上的 errorElement
```
errorElement: <ErrorPage />
```
## 用户界面
创建用户界面 src/routes/contact.jsx 并创建路由（当然希望呈现在 Root 中那么就使用嵌套路由）。
```
{ path: "contacts/:contactId", element: <Contact />, },
```
```jsx
// 子路由
children: [
    {
        path: 'contacts/:contactsId',
        element: <Contact />
    }
]
```
但这样还是不够的，我们需要让 Root 知道 children 应该展示在什么位置。使用 Outlet 来实现~
```jsx
<div id="detail">
    {/*在此处(Outlet)放置子路由*/}
    <Outlet/>
</div>
```
## 客户端路由
客户端路由允许我们的应用程序更新 URL，而无需从服务器请求另一个文档。也就是说不会对链接中的 url 进行完整文档的请求，此时需要我们使用 Link 代替 a 标签。

将`<a href>`全部更改为`<Link to>`。

## 加载数据
这一节主要强调 URL、布局和数据 的解耦。

在根模块文件中创建并导出一个加载器函数，并配置到路由。

getContacts() 是我们自己封装的数据请求 API，新增的数据暂时存储到  localforage。
```jsx
export async function loader() {
    const contacts = await getContacts();
    return { contacts };
}
```
路由配置 loader
```jsx
// 配置 loader
loader: rootLoader,
```
数据渲染，使用 内置的 useLoaderData() 方法
```jsx
const { contacts } = useLoaderData();
```
## 数据写入
提交 HTML 表单时，它会在浏览器中引起导航事件，就像我们点击某个链接一样。而链接和提交表单的唯一的区别在于请求：链接只能更改 URL，而表单还可以更改请求方式（GET 与 POST）和请求体（POST 表单数据）。

如果没有客户端路由（也就是`react-router`这类的client-side routing），浏览器会自动序列化表单数据，并以不同的方式发送给服务器。对于POST请求，数据会作为请求体（request body）发送给服务器；而对于GET请求，数据会作为URLSearchParams（URL查询参数）的形式附加在URL上发送给服务器。

 React Router的行为与此类似，但它不会将请求发送到服务器，而是使用客户端路由并将请求发送到路由操作[`action`](https://baimingxuan.github.io/react-router6-doc/route/action)进行处理。
 
 我们使用 React Router 封装的 [`<Form>`](https://baimingxuan.github.io/react-router6-doc/components/form)，创建用户。
 ```jsx
<Form method="post">
    <button type="submit">New</button>
</Form>
```
```jsx
export async function action() {
    const contact = await createContact();
    return redirect(`/contacts/${contact.id}/edit`);
}
```
像 loader 一样，action 创建后也需要挂载在路由上。`action: rootAction`。

可以简单的理解为 loader 是需要获取数据时触发的，action 则是修改数据时触发的。

创建用户信息，使用 Form submit之后，我们发现Reat Router 的[`<Form>`](https://baimingxuan.github.io/react-router6-doc/components/form) 自动阻止了浏览器发送请求到服务器，而是将其发送到我们指定的路由操作`action`中 。然后自动触发数据重新验证的过程，也就意味着所有使用 useLoaderData 钩子的组件都会自动更新！并且UI会自动与数据保持同步！避免我们使用 `useState` 、 `onSubmit` 和 `useEffect` 这些操作，极大地简化了我们的操作。

## URL 参数
```jsx
path: 'contacts/:contactId',
```
`:contactId` URL 段。冒号 ( `:` ) 为“动态段”。动态段将匹配 URL 该位置上的动态（变化）值，如联系人 ID。我们将 URL 中的这些值称为“URL 参数”，简称 "params"。这些[`params`](https://baimingxuan.github.io/react-router6-doc/route/loader#params)将作为键值对传递给加载器`loader`，值将作为 `params.contactId` 传递（可以在 loader上使用）。

当然，每个组件中的 loader， action 等都需要在路由中进行配置，而且路由通常都有自己的 laoder 等，只不过他们有时可能会相同。
```jsx
export async function loader({ params }) {
    const contact = await getContact(params.contactId);
    return { contact };
}

export async function Contact() {
    const { contact } = useLoaderData();
    // ...
}
```
## 更新数据
```
<input
    placeholder="First"
    aria-label="First name"
    type="text"
    name="first"
    defaultValue={contact.first}
/>
```
如果没有 JavaScript，当提交表单时，浏览器会创建[`FormData`](https://developer.mozilla.org/en-US/docs/Web/API/FormData)对象，并将其设置为请求的主体（body），然后将请求发送到服务器。

如前所述，React Router 阻止了这种默认行为，而是将请求发送到我们自己想要的操作，也就是自己在组件中定义的 `action`中，同时包括[`FormData`](https://developer.mozilla.org/en-US/docs/Web/API/FormData)。表单中的每个字段都可以通过 `formData.get(name)` 访问。

这里还使用[`Object.fromEntries`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/fromEntries)将数据全部收集到一个对象中，这正是后面自定义的 `updateContact` 函数想要的。

```jsx
export async function action({ request, params }) {
    const formData = await request.formData();
    const updates = Object.fromEntries(formData);
    await updateContact(params.contactId, updates);
    return redirect(`/contacts/${params.contactId}`);
}
```
`loader`和`action`都可以[返回`Response`](https://baimingxuan.github.io/react-router6-doc/route/loader#returning-responses)（因为它们都收到了[`Request`](https://developer.mozilla.org/en-US/docs/Web/API/Request)!）。

[`redirect`](https://baimingxuan.github.io/react-router6-doc/fetch/redirect)辅助函数只是为了更方便地返回[`response`](https://developer.mozilla.org/en-US/docs/Web/API/Response)，告诉应用程序更改位置。
## 活动链接样式
```jsx
<NavLink
    to={`contacts/${contact.id}`}
    className={({ isActive, isPending }) =>
        isActive
            ? "active"
            : isPending
                ? "pending"
                : ""
    }
>
```
`<NavLink>` 是一种特殊的 `<Link>` ，它知道自己是否处于 "激活"、"待定 "或 "过渡 "状态。当用户访处于NavLink指定的URL时，isActive将为true。当链接即将被激活时（数据仍在加载中）， `isPending` 将为 true。
## loading 界面
当用户浏览应用时，React Router 会在为下一页加载数据时*保留旧页面*。也就是说这种数据模型具有客户端缓存，因此第二次导航到之前已经访问过的路由时速度会很快。

[`useNavigation`](https://baimingxuan.github.io/react-router6-doc/hooks/use-navigation)返回当前导航状态：可以是`"idle" | "submitting" | "loading"`
```jsx
const navigation = useNavigation();
// ...
navigation.state === "loading" ? "loading" : ""
```
## 删除数据
```jsx
<Form
    method="post"
    action="destroy"
    onSubmit={(event) => {
        if (
            !confirm(
                "Please confirm you want to delete this record."
            )
        ) {
            event.preventDefault();
        }
    }}
>
    <button type="submit">Delete</button>
</Form>
```
`action` 指向 `"destroy"` 。与 `<Link to>` 一样， `<Form action>` 也可以接收一个相对值。由于表单是在 `contact/:contactId` 中呈现的，因此点击 `destroy` 的相对操作将把表单提交到 `contact/:contactId/destroy` ，所以我们只需要在 `contact/:contactId/destroy` 路由下指定删除操作即可。比如：
```jsx
export async function action({ params }) {
    await deleteContact(params.contactId);
    return redirect("/");
}
```
```jsx
{
    path: 'contacts/:contactId/destroy',
    action: destroyAction,
},
```
详细说明，当用户点击提交按钮时：

1.  `<Form>` 会阻止浏览器向服务器发送新 POST 请求的默认行为，而是通过客户端路由创建一个 POST 请求来模拟浏览器的行为
1.  `<Form action="destroy">` 匹配道路新路由 `"contacts/:contactId/destroy"` ，并向其发送请求
1.  在操作重定向后，React Router 会调用页面上所有数据的`loader`，以获取最新值（这就是 "重新验证"）。 `useLoaderData` 返回新值，并导致组件更新！

## 捕获错误
```jsx
export async function action({ params }) {
    await deleteContact(params.contactId);
    throw new Error("oh dang!");
    return redirect("/");
}
```
虽然我这个操作中` return redirect("/");` 根本就不会执行，但这只是为了说明如何捕获错误😁。

这样操作完成之后，发现直接如今了错误页，必须刷新才会重新加载恢复正常显示。

因此需要在路由中进行捕获：
```jsx
{
    path: 'contacts/:contactId/destroy',
    action: destroyAction,
    errorElement: <div>Oops! There was an error.</div>,
},
```
因为销毁路由有自己的`errorElement`，并且是根路由的子路由，因此错误会在此处路由而不是根路由上呈现。这些错误会以最近的 `errorElement` 冒泡。只要在根路径上有一个，添加多少都行。
## 索引路由
如果在父路由的路径上， `<Outlet>` 由于没有子路由匹配，所以没有任何内容可以呈现。可以把索引路由看作是填补这一空白的默认子路由。

定义一个索引路由的页面模块 index。然后在子路由上添加：
```jsx
{ index: true, element: <Index /> },
```
这将告诉路由，当用户位于父路由的确切路径时，路由器将匹配并呈现此路由，以保证在 `<Outlet>` 中没有其他子路由需要呈现时页面不为空。
## 导航
用一个取消功能来说明。
```jsx
const navigate = useNavigate();
```
```jsx
<button
    type="button"
    onClick={() => {
        navigate(-1);
    }}
>Cancel</button>
```
这里的 `<button type="button">` 虽然看似多余，却是防止按钮提交表单的 默认 HTML 行为。所以按钮上没有`event.preventDefault`。

---
以上说明中路由：
```jsx
// 创建路由
const router = createBrowserRouter([
    {
        path: '/',
        // 将<Root>设置为根路由element
        element: <Root/>,
        // 将<ErrorPage>设置为根路由上的errorElement
        errorElement: <ErrorPage/>,
        // 配置 loader
        loader: rootLoader,
        // 配置 action
        action: rootAction,
        // 子路由
        children: [
            { index: true, element: <Index /> },
            {
                path: 'contacts/:contactId',
                element: <Contact/>,
                loader: contactLoader
            },
            {
                path: "contacts/:contactId/edit",
                element: <EditContact/>,
                loader: contactLoader,
                action: editAction
            },
            {
                path: 'contacts/:contactId/destroy',
                action: destroyAction,
                errorElement: <div>Oops! There was an error.</div>,
            },
        ]
    },
])
```

## Form
### 普通搜索框的表单提交
```jsx
<form id="search-form" role="search">
    <input
        id="q"
        className={searching ? "loading" : ""}
        aria-label="Search contacts"
        placeholder="Search"
        type="search"
        // URL 中有 `?q=`
        name="q"
    />
    <div
        id="search-spinner"
        hidden={!searching}
        aria-hidden
    />
    <div
        className="sr-only"
        aria-live="polite"
    ></div>
</form>
```
form 默认为 get 请求，将表单数据放入get 请求的[`URLSearchParams`](https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams)中。如果 method 设为 post，那么将放入 post 主体中。
### Form 表单提交
将 form 改为 Form，使用客户端路由来提交此表单，并可以在现有的加载器中对数据进行操作。

比如：
```jsx
export async function loader({ request }) {
    const url = new URL(request.url);
    const q = url.searchParams.get("q");
    const contacts = await getContacts(q);
    return { contacts, q };
}
```
因为是 GET 请求而不是 POST 请求，所以 React Router 不会调用 `action` 。

提交 GET 表单与点击链接一样：只是 URL 发生了变化。
### 优化表单
1. 如果在搜索后刷新页面，表单字段中应保留输入的值
解决：设置 q 为默认值（q是我们自定义的表单某项的 key）
```jsx
const { contacts, q } = useLoaderData();//使用q
```
```jsx
<input
    id="q"
    className={searching ? "loading" : ""}
    aria-label="Search contacts"
    placeholder="Search"
    type="search"
    name="q"
    defaultValue={q}
/>
```

2. 在搜索后点击返回，列表已不再过滤，表单字段需要清空输入的值
```jsx
useEffect(() => {
    document.getElementById("q").value = q;
}, [q]);
```
>当然这里同时使用 useEffect, useState 也是可以的

3. 在每次按键时进行过滤，而不是在表单明确提交时进行筛选

使用 React Router 自带的 [`useSubmit`](https://baimingxuan.github.io/react-router6-doc/hooks/use-submit) 。

```jsx
const submit = useSubmit();
```
```jsx
<input
    id="q"
    className={searching ? "loading" : ""}
    aria-label="Search contacts"
    placeholder="Search"
    type="search"
    name="q"
    defaultValue={q}
    onChange={(event) => {
        submit(event.currentTarget.form);
    }}
/>
```
现在，当输入时，表格就会自动提交。

`currentTarget` 是事件附加到的 DOM 节点， `currentTarget.form` 是输入的父表单节点。`submit` 函数将序列化并提交传递给它的任何表单。
4. 由于每次按键都会提交表单，所以如果我们输入 "seba "字符，然后用退格键删除它们，历史堆栈中就会出现 7 个新条目😂（长按回退按钮可以查看）。通过将历史记录堆栈中的当前条目*替换*为下一页，而不是推入下一页，来避免这种情况。

```jsx
<input
    id="q"
    className={searching ? "loading" : ""}
    aria-label="Search contacts"
    placeholder="Search"
    type="search"
    name="q"
    defaultValue={q}
    onChange={(event) => {
        //修改：添加replace
        const isFirstSearch = q == null;
        submit(event.currentTarget.form, {
            replace: !isFirstSearch,
        });
    }}
/>
```
## 非导航的数据改变
之前的所有的更改数据都是使用表单导航，在历史堆栈中创建新条目。

如果我们不希望引起导航更改数据，可以使用[`useFetcher`](https://baimingxuan.github.io/react-router6-doc/hooks/use-fetcher)钩子函数。它允许我们与`loaders`和`actions`进行通信，而不会导致导航。
```jsx
const fetcher = useFetcher();
```
```jsx
<fetcher.Form method="post">
    <button
        name="favorite"
        value={favorite ? "false" : "true"}
        aria-label={
            favorite
                ? "Remove from favorites"
                : "Add to favorites"
        }
    >
        {favorite ? "★" : "☆"}
    </button>
</fetcher.Form>
```
有 `method="post"` ，它就会调用`action`。由于没有提供 `<fetcher.Form action="...">` 属性，它将提交到呈现表单的路由。
```jsx
export async function action({ request, params }) {
    let formData = await request.formData();
    return updateContact(params.contactId, {
        favorite: formData.get("favorite") === "true",
    });
}
```
然后在路由中配置 action 即可。
## 局部捕获错误异常
```jsx
export async function loader({ params }) {
    const contact = await getContact(params.contactId);
    if (!contact) {
        throw new Response("", {
            status: 404,
            statusText: "Not Found",
        });
    }
    return { contact };
}
```
这样操作完成之后，我们发现异常页是全局的，所以就需要在路由中配置，**将子路由包裹在无路径路由中**变成局部异常页。
```jsx
{
    path: '/',
    // 将<Root>设置为根路由element
    element: <Root/>,
    // 将<ErrorPage>设置为根路由上的errorElement
    errorElement: <ErrorPage/>,
    // 配置 loader
    loader: rootLoader,
    // 配置 action
    action: rootAction,
    // 子路由
    children: [
        // 添加无路径路由
        {
            errorElement: <ErrorPage />,
            children: [
                { index: true, element: <Index /> },
                {
                    path: 'contacts/:contactId',
                    element: <Contact/>,
                    loader: contactLoader,
                    action: contactAction,
                },
                {
                    path: "contacts/:contactId/edit",
                    element: <EditContact/>,
                    loader: contactLoader,
                    action: editAction
                },
                {
                    path: 'contacts/:contactId/destroy',
                    action: destroyAction,
                    errorElement: <div>Oops! There was an error.</div>,
                },
            ]
        }
    ]
},
```