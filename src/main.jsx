import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import Root, {loader as rootLoader, action as rootAction} from "./routes/root.jsx";
import ErrorPage from "./error-page.jsx";
import Contact, {loader as contactLoader,action as contactAction,} from "./routes/contact.jsx";
import EditContact, {action as editAction} from "./routes/edit.jsx";
import { action as destroyAction } from "./routes/destroy";
import Index from "./routes/index.jsx";

// 创建路由
const router = createBrowserRouter([
    {
        path: '/',
        // 将<Root>设置为根路由element
        element: <Root/>,
        // 将<ErrorPage>设置为根路由上的errorElement
        errorElement: <ErrorPage/>,
        // 配置 loader
        loader: rootLoader,
        // 配置 action
        action: rootAction,
        // 子路由
        children: [
            // 添加无路径路由
            {
                errorElement: <ErrorPage />,
                children: [
                    { index: true, element: <Index /> },
                    {
                        path: 'contacts/:contactId',
                        element: <Contact/>,
                        loader: contactLoader,
                        action: contactAction,
                    },
                    {
                        path: "contacts/:contactId/edit",
                        element: <EditContact/>,
                        loader: contactLoader,
                        action: editAction
                    },
                    {
                        path: 'contacts/:contactId/destroy',
                        action: destroyAction,
                        errorElement: <div>Oops! There was an error.</div>,
                    },
                ]
            }
        ]
    },
])

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        {/* 使用路由 */}
        <RouterProvider router={router}/>
    </React.StrictMode>,
);
