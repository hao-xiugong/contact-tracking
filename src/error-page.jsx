import { useRouteError } from "react-router-dom";

// 创建错误页面组件
export default function ErrorPage() {
    // useRouterError 提供了抛出异常的错误信息
    const error = useRouteError();
    console.error(error);

    return (
        <div id="error-page">
            <h1>Oops!</h1>
            <p>Sorry, an unexpected error has occurred.</p>
            <p>
                {/*导航到不存在的路由时，得到一个"未找到"的错误响应 statusText*/}
                <i>{error.statusText || error.message}</i>
            </p>
        </div>
    );
}